Website Template
=============

Base framework for most websites with admin and custom content running off of Symfony 3 and PHP 7.0

To setup, run the following server script:


A good nginx configuration is as follows and will force SSL:

    server {
        listen 80;
        server_name photoatm.cyint.technology;
        return      301 https://$server_name$request_uri;
    }

    server {
        listen 443 ssl;
        server_name photoatm.cyint.technology;
        root /var/www/photo-atm-web-application/web;

        ssl_certificate /etc/nginx/ssl/photoatm.crt;
        ssl_certificate_key /etc/nginx/ssl/photoatm.key;

        location / {
            # try to serve file directly, fallback to app.php
            try_files $uri /app_dev.php$is_args$args;
        }
        # DEV
        # This rule should only be placed on your development environment
        # In production, don't include this and don't deploy app_dev.php or config.php
        location ~ ^/(app_dev|config)\.php(/|$) {
            fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
            fastcgi_split_path_info ^(.+\.php)(/.*)$;
            include fastcgi_params;
            # When you are using symlinks to link the document root to the
            # current version of your application, you should pass the real
            # application path instead of the path to the symlink to PHP
            # FPM.
            # Otherwise, PHP's OPcache may not properly detect changes to
            # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
            # for more information).
            fastcgi_param  SCRIPT_FILENAME  $realpath_root$fastcgi_script_name;
            fastcgi_param DOCUMENT_ROOT $realpath_root;
        }

            location /phpmyadmin {
                   root /usr/share/;
                   index index.php index.html index.htm;
                   location ~ ^/phpmyadmin/(.+\.php)$ {
                           try_files $uri =404;
                           root /usr/share/;
                       fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
                           fastcgi_index index.php;
                           fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                           include /etc/nginx/fastcgi_params;
                   }
                   location ~* ^/phpmyadmin/(.+\.(jpg|jpeg|gif|css|png|js|ico|html|xml|txt))$ {
                           root /usr/share/;
                   }
            }
 
            location /phpMyAdmin {
               rewrite ^/* /phpmyadmin last;
            }


        error_log /var/log/nginx/photoatm_error.log;
        access_log /var/log/nginx/photoatm_access.log;
    }


clone into a directory, then within that directory run

    composer install
    npm install
    ./node_modules/.bin/gulp
    ./photoatm-server-setup.sh
    
Also, be sure to install the lato true-type font styles in the appropriate font directory for PhantomJS/Emojis to work correctly.
