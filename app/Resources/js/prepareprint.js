$(document).ready(function () {
    initializePrintPrepare();
    modules_loaded.push(initializePrintPrepare);
});

function initializePrintPrepare() {
    var $prepareprint = $('.js-print-prepare');
    var url;
    var redirect;

    if($prepareprint.length > 0) {
        url = $prepareprint.data('url');  
        redirect = $prepareprint.data('redirect');
        error_redirect = $prepareprint.data('error');

        $.ajax({
            url: url
            ,success: function () { window.location = redirect}
            ,error: function () { window.location = error_redirect }
        });
    }
}
