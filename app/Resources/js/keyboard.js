var keyboard_config = {

  // set this to ISO 639-1 language code to override language set by the layout
  // http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
  // language defaults to "en" if not found
  language     : null,  // string or array
  rtl          : false, // language direction right-to-left

  // *** choose layout ***
    display: {
        'bksp'   :  "\u2190",
        'accept' : 'return',
        'normal' : 'ABC',
        'meta1'  : '.?123',
        'meta2'  : '#+=',
        'emoji'  : '😊'
    },

    layout: 'custom',
    customLayout: {
        'normal': [
            'q w e r t y u i o p {bksp}',
            'a s d f g h j k l {accept}',
            '{s} z x c v b n m , . {s}',
            '{meta1} 😊 {space} {meta1}'
        ],
        'shift': [
            'Q W E R T Y U I O P {bksp}',
            'A S D F G H J K L {accept}',
            '{s} Z X C V B N M ! ? {s}',
            '{meta1} {space} {meta1}'
        ],
        'meta1': [
            '1 2 3 4 5 6 7 8 9 0 {bksp}',
            '- / : ; ( ) \u20ac & @ {accept}',
            '{meta2} . , ? ! \' " {meta2}',
            '{normal} {space} {normal}'
        ],
        'meta2': [
            '[ ] { } # % ^ * + = {bksp}',
            '_ \\ | ~ &lt; &gt; $ \u00a3 \u00a5 {accept}',
            '{meta1} . , ? ! \' " {meta1}',
            '{normal} {space} {normal}'
        ]
    },

  position : {
    // optional - null (attach to input/textarea) or a jQuery object
    // (attach elsewhere)
    of : null,
    my : 'center top',
    at : 'center top',
    // used when "usePreview" is false
    // (centers keyboard at bottom of the input/textarea)
    at2: 'center bottom'
  },

  // allow jQuery position utility to reposition the keyboard on window resize
  reposition : true,

  // preview added above keyboard if true, original input/textarea used if false
  usePreview : true,

  // if true, the keyboard will always be visible
  alwaysOpen : false,

  // give the preview initial focus when the keyboard becomes visible
  initialFocus : true,

  // Avoid focusing the input the keyboard is attached to
  noFocus : false,

  // if true, keyboard will remain open even if the input loses focus.
  stayOpen : true,

  // Prevents the keyboard from closing when the user clicks or
  // presses outside the keyboard. The `autoAccept` option must
  // also be set to true when this option is true or changes are lost
  userClosed : false,

  // if true, keyboard will not close if you press escape.
  ignoreEsc : false,

  // if true, keyboard will only closed on click event instead of mousedown or
  // touchstart. The user can scroll the page without closing the keyboard.
  closeByClickEvent : false,

  // *** change keyboard language & look ***
  // Message added to the key title while hovering, if the mousewheel plugin exists
  wheelMessage : 'Use mousewheel to see other keys',

  css : {
    // input & preview
    input          : 'ui-widget-content ui-corner-all',
    // keyboard container
    container      : 'ui-widget-content ui-widget ui-corner-all ui-helper-clearfix',
    // keyboard container extra class (same as container, but separate)
    popup: '',
    // default state
    buttonDefault  : 'ui-state-default ui-corner-all',
    // hovered button
    buttonHover    : 'ui-state-hover',
    // Action keys (e.g. Accept, Cancel, Tab, etc); replaces "actionClass"
    buttonAction   : 'ui-state-active',
    // used when disabling the decimal button {dec}
    buttonDisabled : 'ui-state-disabled',
    // empty button class name {empty}
    buttonEmpty    : 'ui-keyboard-empty'
  },

  // *** Useability ***
  // Auto-accept content when clicking outside the keyboard (popup will close)
  autoAccept : false,
  // Auto-accept content even if the user presses escape
  // (only works if `autoAccept` is `true`)
  autoAcceptOnEsc : false,

  // Prevents direct input in the preview window when true
  lockInput : false,

  // Prevent keys not in the displayed keyboard from being typed in
  restrictInput : false,
  // Additional allowed characters while restrictInput is true
  restrictInclude : '', // e.g. 'a b foo \ud83d\ude38'

  // Check input against validate function, if valid the accept button
  // is clickable; if invalid, the accept button is disabled.
  acceptValid : true,
  // Auto-accept when input is valid; requires `acceptValid`
  // set `true` & validate callback
  autoAcceptOnValid : false,

  // if acceptValid is true & the validate function returns a false, this option
  // will cancel a keyboard close only after the accept button is pressed
  cancelClose : true,

  // Use tab to navigate between input fields
  tabNavigation : false,

  // press enter (shift-enter in textarea) to go to the next input field
  enterNavigation : true,
  // mod key options: 'ctrlKey', 'shiftKey', 'altKey', 'metaKey' (MAC only)
  // alt-enter to go to previous; shift-alt-enter to accept & go to previous
  enterMod : 'altKey',

  // if true, the next button will stop on the last keyboard input/textarea;
  // prev button stops at first
  // if false, the next button will wrap to target the first input/textarea;
  // prev will go to the last
  stopAtEnd : true,

  // Set this to append the keyboard immediately after the input/textarea it
  // is attached to. This option works best when the input container doesn't
  // have a set width and when the "tabNavigation" option is true
  appendLocally : false,

  // Append the keyboard to a desired element. This can be a jQuery selector
  // string or object
  appendTo : 'body',

  // If false, the shift key will remain active until the next key is (mouse)
  // clicked on; if true it will stay active until pressed again
  stickyShift : true,

  // caret placed at the end of any text when keyboard becomes visible
  caretToEnd : false,

  // Prevent pasting content into the area
  preventPaste : false,

  // caret stays this many pixels from the edge of the input
  // while scrolling left/right; use "c" or "center" to center
  // the caret while scrolling
  scrollAdjustment : 10,

  // Set the max number of characters allowed in the input, setting it to
  // false disables this option
  maxLength : false,

  // allow inserting characters @ caret when maxLength is set
  maxInsert : true,

  // Mouse repeat delay - when clicking/touching a virtual keyboard key, after
  // this delay the key will start repeating
  repeatDelay : 500,

  // Mouse repeat rate - after the repeatDelay, this is the rate (characters
  // per second) at which the key is repeated. Added to simulate holding down
  // a real keyboard key and having it repeat. I haven't calculated the upper
  // limit of this rate, but it is limited to how fast the javascript can
  // process the keys. And for me, in Firefox, it's around 20.
  repeatRate : 20,

  // resets the keyboard to the default keyset when visible
  resetDefault : false,

  // Event (namespaced) on the input to reveal the keyboard. To disable it,
  // just set it to an empty string ''.
  openOn : 'focus',

  // When the character is added to the input
  keyBinding : 'mousedown touchstart',

  // enable/disable mousewheel functionality
  // enabling still depends on the mousewheel plugin
  useWheel : true,

  // combos (emulate dead keys)
  // http://en.wikipedia.org/wiki/Keyboard_layout#US-International
  // if user inputs `a the script converts it to à, ^o becomes ô, etc.
  useCombos : true,

  // *** Methods ***
  // Callbacks - add code inside any of these callback functions as desired
  initialized   : function(e, keyboard, el) {},
  visible       : function(e, keyboard, el) {},
  beforeClose   : function(e, keyboard, el, accepted) {},
  restricted    : function(e, keyboard, el) {},
  hidden        : function(e, keyboard, el) {},

  // called instead of base.switchInput
  switchInput : function(keyboard, goToNext, isAccepted) {},

  // used if you want to create a custom layout or modify the built-in keyboard
  create : function(keyboard) { return keyboard.buildKeyboard(); },

  // build key callback (individual keys)
  buildKey : function( keyboard, data ) {
    /*
    data = {
      // READ ONLY
      // true if key is an action key
      isAction : [boolean],
      // key class name suffix ( prefix = 'ui-keyboard-' ); may include
      // decimal ascii value of character
      name     : [string],
      // text inserted (non-action keys)
      value    : [string],
      // title attribute of key
      title    : [string],
      // keyaction name
      action   : [string],
      // HTML of the key; it includes a <span> wrapping the text
      html     : [string],
      // jQuery selector of key which is already appended to keyboard
      // use to modify key HTML
      $key     : [object]
    }
    */
    return data;
  },

  // this callback is called just before the "beforeClose" to check the value
  // if the value is valid, return true and the keyboard will continue as it
  // should (close if not always open, etc)
  // if the value is not value, return false and the clear the keyboard value
  // ( like this "keyboard.$preview.val('');" ), if desired
  // The validate function is called after each input, the "isClosing" value
  // will be false; when the accept button is clicked, "isClosing" is true
  validate : function(keyboard, value, isClosing) {       
    return true;
    }

}


$(document).ready(function(){
    initializeKeyboard();
    if(modules_loaded) {
        modules_loaded.push(initializeKeyboard);
    }
});

function initializeKeyboard() {

    var $entry = $('.js-entry');
    var $entrytrigger = $('.js-entry-trigger');

    if($entry.length > 0) {

        $entryclose = $('.glyphicon-remove', '.ui-keyboard-preview');
        $entry.each(function() {
            var $entry_ref = $(this);
            var mode = $entry_ref.data('mode');
           
            keyboard_config.beforeVisible = function(e, keyboard, el) { toggleEntryBar(e, $entry_ref); };
            keyboard_config.accepted = function(e, keyboard, el) { keyPressEntry(e, $entry_ref); };
            keyboard_config.canceled = function(e, keyboard, el) { keyPressEntry(e, $entry_ref)};
            keyboard_config.beforeInsert  = function(e, keyboard, el, textToAdd) { 
              var $input = $('.ui-keyboard-preview-wrapper input');
              if(textToAdd == '😊' && mode != 'instagram') {
                     
                    $input.emojiPicker({
                        width: '300px',
                        height: '200px',
                        button: false
                      });

                    $input.emojiPicker('toggle');
              } else {
                    return textToAdd; 
              }
            };
            keyboard_config.change = function(e, keyboard, el) {
                var $entrybar = $('.ui-keyboard-preview-wrapper input');
                $entry_ref.val($entrybar.val());  
            };

            $(this).keyboard(keyboard_config);       
        });  
    }    

    if($entrytrigger.length > 0) {
        $entrytrigger.click(function(event) {entryTrigger(event, $(this));});        
    }
}

function entryTrigger(event, $button) {
    var target = $button.data('target');
    var toggled = $button.data('toggled') || false;
    if(toggled) {
        toggleData(event, $button);
    } else {           
        $(target).focus();                
    }
}

function hideEntryBar(event, $button) {
    var $entrybar = $('.ui-keyboard-preview-wrapper input');
    var $entrybarcontainer = $('.ui-keyboard-preview-wrapper');
    $button.fadeIn(250);
    $entrybar.attr('placeholder','');
    $entrybar.val('');
    $entrybar.removeClass();
    $entrybar.addClass('text-entry');
    $entrybar.addClass('with-icon');
    $('body').focus();
}

function toggleEntryBar(event, $button) {
    var $entrybar = $('.ui-keyboard-preview-wrapper input');
    var $entrybarcontainer = $('.ui-keyboard-preview-wrapper');
    var mode = $button.data('mode');
    var placeholder = $button.data('placeholder');
    var value = $button.data('value') || '';
/*    $entrybar.off('keypress');
    $entrybar.on('keypress', function(event) { keyPressEntry(event, $button, $(this)); });
*/
    $entrybar.val('');
    $entrybar.removeClass();
    $entrybar.addClass('text-entry');
    $entrybar.addClass('with-icon');
    $entrybar.addClass(mode);
    $entrybar.attr('placeholder', placeholder);
    $entrybar.val(value);
}

function keyPressEntry(event, $button) {
    var url;
    var value = $button.val() || ''; 
    var target;
    var mode = $button.data('mode');
    var $input = $('.ui-keyboard-preview-wrapper input');

    if(mode != 'instagram')
        $input.emojiPicker('destroy');

    if(mode != 'search' && mode != 'instagram') {
        url = $button.data('url');
        target = $button.data('id');
        $.ajax({
            url: url
            ,method: 'post'
            ,data: {value: value}
            ,success: function(data) { modifyMediaData(data, $button, $input);}
            ,error: function (data) {  
                if(data.length > 0) {
                    addMessage(data[0].text, data[0].type, data[0].icon);                
                }   
            }
        });        

        hideEntryBar(event, $button);
    } else if (mode != 'instagram') {
        $('#search_form').submit();        
    }  else {
    } 
}

