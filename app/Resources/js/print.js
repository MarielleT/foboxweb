$(document).ready(function(){
    initializePrint();
    modules_loaded.push(initializePrint);
});

function initializePrint() {
    var $print = $('#print');
    var time, rooturl;
    if($print.length > 0) {
        time = $print.data('time');
        rooturl = $print.data('rooturl');
        animatePrintProgress(time, rooturl);
    }
}

function animatePrintProgress(time, rooturl) {
    var counter = 0;
    var $progressbar = $('.progress-bar');
    var $imagedisplay = $('.photo-strip');
    var $time = $('#time');
    var remaining;
    var slides = $imagedisplay.data('photos');     
    var interval = setInterval(function(){
        var totalslides = slides.length;
        var currslide = 0;
        var ratio = (time > 0 ? (counter/time) : 0); 
        var percent = ratio  * 100;
        currslide = Math.floor(ratio * (totalslides));
        $imagedisplay.attr('src',slides[currslide]);
        remaining = time - counter;
        remaining = remaining > 0 ? remaining : 0; 
        $progressbar.width((ratio < 100 ? (percent) : '100') + '%');
        counter+=250;
        $time.text(Math.ceil(remaining/1000));
        
        if(counter >= time+5000) {
            clearInterval(interval);
            window.location = rooturl;
        }
    }, 250);
    console.log(slides);
}
