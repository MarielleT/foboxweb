$(document).ready(function(){    
    initializeImageCrop();
    modules_loaded.push(initializeImageCrop);
});

function cropCarouselImage () {
    var $item = $('#carousel-photos .active .photo');
    $(this).off('update');
    $(this).on('update', function(event, croppe) {
        var crop, zoom;
        crop = croppe.points;
        zoom = croppe.zoom;
        $item.data('crop',crop);
        $item.data('zoom',zoom); 
    });

    cropImage($item, 0);
}

function initializeImageCrop() {
    var $cropimages = $('.cropped');
    var crop, zoom, params = {};
    if($cropimages.length > 0) {
        $cropimages.each(function(index){
            crop = $(this).data('crop') || null;
            zoom = $(this).data('zoom') || null;
            if(crop || zoom) {
                params = {
                    points: crop
                    ,zoom: zoom
                };
            }
 
            cropImage($(this), params);
        }); 
    }


    $('#carousel-photos').on('carousel_show', cropCarouselImage);   
	$('#carousel-photos').on('slid.bs.carousel', cropCarouselImage);
    $('#carousel-photos').on('carousel_hide', function () {
        var crop, zoom, slot, url;
        crop = $('.item.active .photo', $(this)).data('crop');
        zoom = $('.item.active .photo', $(this)).data('zoom');
        slot = $('.item.active .photo', $(this)).data('slot');
        url = $('.item.active .photo', $(this)).data('cropurl');
        $.ajax({
            url: url
            ,method: 'post'
            ,data: {
                value: {
                        crop: crop
                        ,zoom: zoom
               }
            }
            ,success: function () {
                $('#media_card_' + slot + ' .photo').croppie('destroy');
                cropImage($('#media_card_' + slot + ' .photo'), {points:crop, zoom:zoom, slot:slot});
            }
            ,error: function () {}
        });
    } );

}

function refreshCrop($this) {
    $this.croppie('destroy');
    $this.data('crop', null);
    $this.data('zoom', null);
    cropImage($this);
}

function cropImage($this, params) {
    var opts = $this.data('cropopts');    
    params = params || {};
    params.url = $this.data('url');
    if($this.is(':visible') && !$this.hasClass('croppie-container')) {                    
        $this.croppie(opts);
        $this.croppie('bind', params);
    }
}


