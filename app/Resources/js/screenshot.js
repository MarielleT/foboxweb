var casper = require("casper").create({verbose: true});

casper.start(casper.cli.args[0], function() {

/*    var js = this.evaluate(function() {
        return document; 
    }); 
    this.echo(js.all[0].outerHTML); 
*/
    if(!casper.exists(casper.cli.args[2])) {
        casper.log('Does not exist', 'error');   
    } else {
        casper.log('Selector found', 'info');   
        this.captureSelector(casper.cli.args[1], casper.cli.args[2]);
    }
});

casper.run();
