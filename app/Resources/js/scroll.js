window.onload = function () {
    initializeScroll();
    modules_loaded.push(initializeScroll);
}

function initializeScroll() {
    'use strict';

    var view, indicator, relative,
        min, max, offset, reference, pressed, xform,
        velocity, frame, timestamp, ticker,
        amplitude, target, timeConstant, $view, mode;

    function ypos(e) {
        // touch event
        if (e.targetTouches && (e.targetTouches.length >= 1)) {
            return e.targetTouches[0].clientY;
        }

        // mouse event
        return e.clientY;
    }

    function xpos(e) {
        // touch event
        if (e.targetTouches && (e.targetTouches.length >= 1)) {
            return e.targetTouches[0].clientX;
        }

        // mouse event
        return e.clientX;
    }


    function scroll(y) {
        offset = (y > max + 1000) ? max + 1000 : (y < min) ? min : y;
        $view[0].style[xform] = 'translateY(' + (-offset) + 'px)';
        indicator.style[xform] = 'translateY(' + (offset * relative) + 'px)';
    }

    function scrollX(x) {
        offset = (x > max + 1) ? max + 1 : (x < min) ? min : x;
        $view[0].style[xform] = 'translateX(' + (-offset) + 'px)';
        indicator.style[xform] = 'translateX(' + (offset * relative) + 'px)';
    }

    function track() {
        var now, elapsed, delta, v;

        now = Date.now();
        elapsed = now - timestamp;
        timestamp = now;
        delta = offset - frame;
        frame = offset;

        v = 1000 * delta / (1 + elapsed);
        velocity = 0.8 * v + 0.2 * velocity;
    }

    function autoScroll() {
        var elapsed, delta;
        mode = $view.data('mode'); 
        if (amplitude) {
            elapsed = Date.now() - timestamp;
            delta = -amplitude * Math.exp(-elapsed / timeConstant);
            if (delta > 0.5 || delta < -0.5) {
                if(mode) {
                    scrollX(target + delta);
                } else {
                    scroll(target + delta);
                }
                requestAnimationFrame(autoScroll);
            } else {
                if(mode) {
                    scrollX(target);
                } else{
                    scroll(target);
                }
            }
        }
    }

    function tap(e) {
        pressed = true;
        reference = ypos(e);

        velocity = amplitude = 0;
        frame = offset;
        timestamp = Date.now();
        clearInterval(ticker);
        ticker = setInterval(track, 100);

        e.preventDefault();
        e.stopPropagation();
        return false;
    }

    function tapX(e) {
        pressed = true;
        reference = xpos(e);

        velocity = amplitude = 0;
        frame = offset;
        timestamp = Date.now();
        clearInterval(ticker);
        ticker = setInterval(track, 100);

        e.preventDefault();
        e.stopPropagation();
        return false;
    }


    function drag(e) {
        var y, delta;
        if (pressed) {
            y = ypos(e);
            delta = reference - y;
            if (delta > 2 || delta < -2) {
                reference = y;
                scroll(offset + delta);
            }
        }
        e.preventDefault();
        e.stopPropagation();
        return false;
    }

    function dragX(e) {
        var x, delta;
        if (pressed) {
            x = xpos(e);
            delta = reference - x;
            if (delta > 2 || delta < -2) {
                reference = x;
                scrollX(offset + delta);
            }
        }
        e.preventDefault();
        e.stopPropagation();
        return false;
    }


    function release(e) {
        pressed = false;

        clearInterval(ticker);
        if (velocity > 10 || velocity < -10) {
            amplitude = 0.8 * velocity;
            target = Math.round(offset + amplitude);
            timestamp = Date.now();
            requestAnimationFrame(autoScroll);
        }

        e.preventDefault();
        e.stopPropagation();
        return false;
    }

    $view = $('#view');
    if($view.length > 0) {
/*        if (typeof window.ontouchstart !== 'undefined') {
            $view.on('touchstart', tap);
            $view.on('touchmove', drag);
            $view.on('touchend', release);
        } */
        mode = $view.data('mode');

        if(mode) {       
            $view.off('mousemove');
            $view.on('mousemove', dragX);
            $view.off('mousedown');
            $view.on('mousedown', tapX);
        } else {
            $view.off('mousemove');
            $view.on('mousemove', drag);
            $view.off('mousedown');
            $view.on('mousedown', tap);
        }

        $view.on('mouseup', release);

        if(mode) {
            max = parseInt(getComputedStyle($view[0]).width, 10) - innerWidth;
        } else {
            max = parseInt(getComputedStyle($view[0]).height, 10) - innerHeight;
        }
        offset = min = 0;
        pressed = false;
        timeConstant = 325; // ms

        indicator = document.getElementById('indicator');
        if(mode) {
            relative = (innerWidth - 30) / max;
        } else {
            relative = (innerHeight - 30) / max;
        }

        xform = 'transform';
        ['webkit', 'Moz', 'O', 'ms'].every(function (prefix) {
            var e = prefix + 'Transform';
            if (typeof $view[0].style[e] !== 'undefined') {
                xform = e;
                return false;
            }
            return true;
        });
    }
};

/*
Copyright (C) 2013 Ariya Hidayat

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
