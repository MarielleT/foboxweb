function hookup_delete_cart_item_event() {
    var $ok = $('#ok');
    var $cancel = $('#cancel');
    var $modal = $('#main_modal');
    $ok.click(function(event) {deleteCartItem(event, $(this))});
    $cancel.click(function(event) { $modal.modal('hide'); } );
} 

function deleteCartItem(event, $button) {
    var id = $button.data('id');
    var slot = $button.data('slot');
    var url = $button.data('deleteurl');
    var redirect = $button.data('redirecturl');
    var redirect_cart = $button.data('redirectcarturl');
    $.ajax({
        url: url
        ,method: 'post'
        ,data: {slot:slot}
        ,success: function(data) { successDeleteCartItem(data, redirect, redirect_cart); }
        ,error: function(data) { errorDeleteCartItem(data); }
    });
}

function successDeleteCartItem(data, redirect, redirect_cart) {
    if(data.success) {
        $modal = $('#main_modal').modal('hide');
        if(data.data.cart_data.total <= 0) {
            window.location = redirect_cart;
        } else {
            window.location = redirect;
        }
    } else {
        addMessage(data.data[0].text, data.data[0].type, data.data[0].icon);  
    }

}

function errorDeleteCartItem(data) {
	addMessage('An error occurred while attempting to delete this photo from your order.', 'danger', 'exclamation-mark');  
}
