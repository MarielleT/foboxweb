<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160908201442 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $env = $this->container->get('kernel')->getEnvironment(); 
        if($env == 'dev')
        {
            $this->addSql(
<<<'EOT'
                INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `created`) VALUES (NULL,'admintest', 'admintest', 'ZGpoZmg4MmhzZ2RpZmpheBW6nkBpNZJ9wKDNbyhLY7wA0ck2yIky6oMh3KwAyJNc', 'ZGpoZmg4MmhzZ2RpZmpheBW6nkBpNZJ9wKDNbyhLY7wA0ck2yIky6oMh3KwAyJNc', '1', 'ag1xm2p8oo0gok8w8ko48s8cggco4so', '$2y$13$MuC89c0T9imBoVn9c2lsAuKCIWHxsPz94VyYRE4DZ4j2X2cUTnuuW', '2016-06-05 00:54:02', '0', '0', NULL, NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', '0', NULL, UNIX_TIMESTAMP())
EOT
            );            
        }
        else
        {
            $this->addSql(
<<<'EOT'
                INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `created`) VALUES (NULL,'admin', 'admin', 'ZGpoZmg4MmhzZ2RpZmpheBW6nkBpNZJ9wKDNbyhLY7wA0ck2yIky6oMh3KwAyJNc', 'ZGpoZmg4MmhzZ2RpZmpheBW6nkBpNZJ9wKDNbyhLY7wA0ck2yIky6oMh3KwAyJNc', '1', 'ag1xm2p8oo0gok8w8ko48s8cggco4so', '$2y$13$Wsk3y9Xq/8J2coWAhziGB.R4ra7GgT3SDqA73UANwrShZiHuCdatq', '2016-06-05 00:54:02', '0', '0', NULL, NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', '0', NULL, UNIX_TIMESTAMP())
EOT
            );
        }

        $this->addSql(
<<<'EOT'
                INSERT INTO `custom_content` (`content`,`created`,`friendly_url`,`id`,`published`,`title`,`updated`) 
                VALUES 
                     ('',UNIX_TIMESTAMP(),'index',1,1,'index',NULL)
                    ,('',UNIX_TIMESTAMP(),'terms-of-service',2,1,'Terms of Service',NULL)
                    ,('',UNIX_TIMESTAMP(),'privacy-policy',3,1,'Privacy Policy',NULL)
EOT
            );

        $this->addSql(
<<<'EOT'
                INSERT INTO `navigation` (`name`,`created`) 
                VALUES 
                    ('Header', UNIX_TIMESTAMP())
                    ,('Footer', UNIX_TIMESTAMP())

EOT
            );


        $this->addSql(
<<<EOT
                INSERT INTO `setting` (`setting_key`,`value`) 
                VALUES
                    ('checkout_currency','USD'),
                    ('checkout_photo_fee',2.00),
                    ('checkout_helpline', '+34 666 876 465'),
                    ('checkout_max_quantity',4),
                    ('credentials_hopper_authtoken', '')
                    ('credentials_server', '')
                    ('credentials_username', '')
                    ('credentials_password','')
                    ('credentials_database','')
                    ('instagram_client_secret','f409a6a9af7e423f9264469e5cef29b0'),
                    ('instagram_client_id','3f5a053adf944de89108210cbd91ee4c'),
                    ('instagram_base_access_token', '2351426252.3f5a053.d50a5a982c9e4e56bbf5ecdf4e453ed0'),
                    ('instagram_base_url', 'https://api.instagram.com'),
                    ('instagram_redirect_url', 'https://photoatm.cyint.technology/en/instagram/authorized'),
                    ('paginator_items_per_page',6),
                    ('ga_account',''),
                    ('html_header',''),
                    ('html_footer','')
EOT
            );            

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
