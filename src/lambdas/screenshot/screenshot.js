var casper = require("casper").create({verbose: true});

casper.start(casper.cli.args[0], function() {

    if(!casper.exists(casper.cli.args[2])) {    
        this.exit();
    } else {      
        this.captureSelector('/tmp/' + casper.cli.args[1], casper.cli.args[2]);
        this.then(function() {          
            this.exit();
        });
    }
  
});

casper.run();
