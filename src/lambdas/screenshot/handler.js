'use strict';
var AWS = require('aws-sdk');
var fs = require('fs');
const s3 = new AWS.S3();

module.exports.screenshot = (event, context, callback) => {
  var params = event.body;
  var path = require('path');
  var childProcess = require('child_process');
  var casperPath = path.join(__dirname, 'node_modules/casperjs/bin/casperjs');
  var media_url = params.render_url;
  var selector = params.selector;
  var destination = params.destination;
  var transaction = params.transaction;
  var childArgs = [
      path.join(__dirname,'screenshot.js'),
      media_url, destination, selector
  ];

  process.env['PATH'] = process.env['PATH'] + ':' + process.env['LAMBDA_TASK_ROOT'];
  process.env['PHANTOMJS_EXECUTABLE'] = path.join(__dirname, 'node_modules/phantomjs-prebuilt/bin/phantomjs'); 

  childProcess.execFile(casperPath, childArgs, function(err, stdout, stderr) {   
    var response;
    fs.readFile('/tmp/' + destination, function(err, data) {
        if (err) throw err;
       
        s3.upload({
            Bucket: 'photoatm'
            ,ACL: 'public-read'
            ,ContentType: 'image/png'
            ,Key: destination
            ,Body: data
        }, function(err, data) {
            fs.unlink('/tmp/' + destination, function(err) {
                response = (err | stderr) ? {
                    statusCode: 500,
                    body: (err | stderr)
                }
                : {
                  statusCode: 200,
                  headers: {
                    "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
                    "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS
                  },
                  body: {message:'success'}
                };

             });

             callback(null, response);

        });                 
    }); 
 }) 
};

module.exports.reauth = (event, context, callback) => {
  var params = JSON.parse(event.body);
  var path = require('path');
  var childProcess = require('child_process');
  var casperPath = path.join(__dirname, 'node_modules/casperjs/bin/casperjs');
  var instagram_url = params.url;
  var username = params.username;
  var password = params.password;
  var childArgs = [
      path.join(__dirname,'reauth.js'),
      instagram_url, username, password
  ];

  process.env['PATH'] = process.env['PATH'] + ':' + process.env['LAMBDA_TASK_ROOT'];
  process.env['PHANTOMJS_EXECUTABLE'] = path.join(__dirname, 'node_modules/phantomjs-prebuilt/bin/phantomjs'); 

  childProcess.execFile(casperPath, childArgs, function(err, stdout, stderr) {   
    var response;
    response = (err | stderr) ? {
        statusCode: 500,
        body: (err | stderr)
    }
    : {
      statusCode: 200,
      headers: {
        "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
        "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS
      },
      body: {message:'success'}
    };

    callback(null, stdout);
  });                 
};
