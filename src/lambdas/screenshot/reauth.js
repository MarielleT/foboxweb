var casper = require("casper").create({verbose: true});
var args = casper.cli.args;

var instagram_url = args[0];
var username = args[1];
var password = args[2];

casper.start(instagram_url, function() {
	this.waitForText('Log in');   
    this.then(function() { this.sendKeys('input[type="text"]', username); });
    this.then(function() { this.sendKeys('input[type="password"]', password); });
    this.then(function() { this.click('input[type="submit"]'); });
    this.then(function() { 

        this.waitForSelector('button.confirm', function() {
            this.click('button.confirm');   
        }, function () {
            console.log(this.getCurrentUrl());
            this.exit();
        });
    });
   
});

casper.run();
