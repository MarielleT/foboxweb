<?php

namespace AppBundle\Repository;
use AppBundle\Factory\MachineFactory;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MachineRepository extends \Doctrine\ORM\EntityRepository
{
    public function findByFilter($filter = null)
    {
        $query = $this->createQueryBuilder('m');
        if(!empty($filter))
        {
            $string = empty($filter['string']) ? null : $filter['string'];
            $status = empty($filter['status']) ? [] : $filter['status'];

            if(!empty($string))
            {
                $query
                    ->andWhere('m.id like :filter or m.address like :filter or m.city like :filter or m.postal_code like :filter or m.country like :filter')
                    ->setParameter(':filter', "%$string%");
            }

            if($status !== null)
            {         
                foreach($status as $key=>$stat)
                {
                    $query
                        ->orWhere('m.status like :stat' . $key)
                        ->setParameter(':stat' . $key, '%' . $stat . '%');
                }                   
            }

        }

        return $query->getQuery()->getResult();
    }

    public function getFactory($Doctrine)
    {
        return new MachineFactory($this, $Doctrine, $Doctrine->getManager());
    }

}
