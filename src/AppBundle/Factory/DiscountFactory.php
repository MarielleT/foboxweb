<?php

namespace AppBundle\Factory;

use CYINT\ComponentsPHP\Classes\ParseData;
use AppBundle\Entity\Discount;

class DiscountFactory extends PhotoATMMasterFactory
{

    protected $fieldKeys = ['code','type','amount','active'];
    protected $EntityType = 'AppBundle\Entity\Discount';

    public function __construct($Repository, $Doctrine, $Manager)
    {
        $this->setDoctrine($Doctrine);
        //Case mut match that used in the getter and setter method as 'get'and 'set' wll be appended to the keys.
         $this->setFields([
            'Id' => $this->initializeField(
                'none', null, null, null, null
            )
            ,'Code' => $this->initializeField(
                'text', 'Code','','',['required'] 
            )
            ,'Type' => $this->initializeField(
                'select', 'Type of Discount', '', '', ['required']
                ,$this->getTypeOptions()
            )
            ,'Amount' => $this->initializeField(
                'number', 'Amount', 0, 0, ['required'], ['step'=>0.01]
            )
            ,'Minimum' => $this->initializeField(
                'number', 'Minimum', 0, 0, ['required'], ['step'=>0.01]
            )
            ,'Active' => $this->initializeField(
                'checkbox', 'Active', false, false, [], null
            )
        ]);

        parent::__construct($Repository, $Doctrine, $Manager);
    }

    public function getExceptionMessage(\Exception $Ex = null)
    {     
        switch(get_class($Ex))
        {
            case 'Doctrine\DBAL\Exception\UniqueConstraintViolationException':
                return 'A discount with this code already exists, and discount codes must be unique. Please enter a new code and try again.';
            break;

            default:
                return $Ex->getMessage();
            break; 
        }
    }

    public function getTypeOptions()
    {
         return [
            'selectOptions' => [
                'type' => 'static'
                ,'options' => [
                    'Amount' => 1
                    ,'Percentage' =>  2
                    ,'Buy minimum get amount free' => 3
                ]
            ]
        ];

    }
    
}
