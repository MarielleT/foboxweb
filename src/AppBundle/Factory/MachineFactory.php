<?php

namespace AppBundle\Factory;

use CYINT\ComponentsPHP\Classes\ParseData;
use AppBundle\Entity\Machine;

class MachineFactory extends PhotoATMMasterFactory
{
    protected $fieldKeys = ['address', 'city','postal_code','province','country'];
    protected $EntityType = '\AppBundle\Entity\Machine';
    public function __construct($Repository, $Doctrine, $Manager)
    {
        $this->setDoctrine($Doctrine);
  
         //Case mut match that used in the getter and setter method as 'get'and 'set' wll be appended to the keys.
         $this->setFields([
            'Id' => $this->initializeField(
                'none', null, null, null, null
            )
            ,'Address' => $this->initializeField(
                'text', 'Address','','',null
            )
            ,'City' => $this->initializeField(
                'text', 'City','','',null
            )
            ,'PostalCode' => $this->initializeField(
                'text', 'PostalCode','','',null
            )
            ,'Province' => $this->initializeField(
                'text', 'Province','','',null
            )
            ,'Country' => $this->initializeField(
                'text', 'Country','','',null
            )
            ,'IPAddress' => $this->initializeField(
                'text', 'IP Address', '','',null
            )
            ,'SpaceRemaining' => $this->initializeField(
                'number', 'Bill Collection Space (# bills)', 0, 0, null
            )
            ,'ChangeRemaining' => $this->initializeField(
                'custom', 'Hopper Contents', '', '', null, ['template'=>'billQuantity']
            )
            ,'Paper' => $this->initializeField(
                'number', 'Prints', '', '', null
            )
            ,'Status' => $this->initializeField(
                'select', 'Status', -1, -1, ['required'], 
                $this->getStatusOptions()
            )           
            ,'Notes' => $this->initializeField(
                'textarea','Notes','','',null
            )
            ,'Created' => $this->initializeField(
                'datedisplay', 'Created', '','', null, ['format' => 'DD.MM.YY hh:mm']
            )
        ]);

        parent::__construct($Repository, $Doctrine, $Manager);
    }

    private function getStatusOptions()
    {
        return [
            'selectOptions' => [
                'type' => 'static'
                ,'multiple' => true
                ,'options' => Machine::getStatusOptions()               
            ]
        ];
    }



}
