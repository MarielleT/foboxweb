<?php

namespace AppBundle\Factory;

use CYINT\ComponentsPHP\Classes\ParseData;
use AppBundle\Entity\Transaction;

class TransactionFactory extends PhotoATMMasterFactory
{

    public function constructEntityFromData($form_data, &$Transaction = null, $_locale = 'en', $parentid = null)
    {
        $fields = [];
        $user_id = null;
        $stripe_auth_id = null;
        $stripe_data = null;
        $answer = null;
        $amount = 0.00;
        $values = null;
        $notes = null;
        $status = 0;
        $paypal_data = null;
        $paypal_auth = null;

        $user_id = ParseData::setArray($form_data, 'user_id', null);
        $stripe_auth_id = ParseData::setArray($form_data, 'stripe_auth_id', null);  
        $paypal_auth = ParseData::setArray($form_data, 'paypal_auth', null); 
        $answer = ParseData::setArray($form_data, 'answer', null);
        $amount = ParseData::setArray($form_data, 'amount', 0.00);
        $values = ParseData::setArray($form_data, 'values', null);
        $notes = ParseData::setArray($form_data, 'notes', null);
        $notes = ParseData::setArray($form_data, 'notes', null);
        $refunded = !empty(ParseData::setArray($form_data, 'refunded', false));

        $status = empty($refunded) ? 0 : 1;

        if(empty($user_id))
            throw new \Exception('A user must be selected for this transaction.');

        if(empty($values['items']))
            throw new \Exception('A form must be associated with this transaction.');

        $cart_data = ['total'=>$amount, 'cart'=>$values];

        $User = $this->getDoctrine()->getRepository('AppBundle:User')->find($user_id);

        if(empty($User))
            throw new \Exception('Invalid user selected.');    

        if(empty($Transaction))
        {
            $Transaction = new Transaction($amount, $cart_data, $stripe_data, $stripe_auth_id, $paypal_data, $paypal_auth, $User);             
        }
        else          
        { 
            $Transaction->setUser($User);
            $Transaction->setStripeAuth($stripe_auth_id);
            $Transaction->setAmount($amount);
            $Transaction->setValues($cart_data);
            $Transaction->setPaypalAuth($paypal_auth);
        }

        $Transaction->setStatus($status);
        $Transaction->setAnswer($answer);
        $Transaction->setNotes($notes);

        $stripe_data = $Transaction->getStripeData();
 
        $fields = [
            'id' => $Transaction->getId(),
            'user_id' => $user_id,
            'stripe_auth_id' => $stripe_auth_id,
            'stripe_data' => $stripe_data,
            'paypal_auth' => $paypal_auth,
            'answer' => $answer,
            'amount' => $amount,
            'values' => $cart_data,
            'notes' => $notes,
            'status' => $status
        ];

        return $fields;       
    }

    public function getEntityFieldData($_locale = 'en', $Transaction, $parentid = null)
    {
        $fields = [];
        $id = null;
        $user_id = null;
        $stripe_auth_id = null;
        $stripe_data = null;
        $answer = null;
        $amount = 0.00;
        $values = null;
        $notes = null;
        $status = null;

        if(!empty($Transaction))
        {
            $id = $Transaction->getId();
            $user_id = $Transaction->getUser()->getId();
            $stripe_auth_id = $Transaction->getStripeAuth();
            $stripe_data = $Transaction->getStripeData();
            $paypal_auth = $Transaction->getPaypalAuth();
            $paypal_data = $Transaction->getPaypalData();

            $answer = $Transaction->getAnswer();
            $amount = $Transaction->getAmount();
            $values = $Transaction->getValues();
            $notes = $Transaction->getNotes();
            $status = $Transaction->getStatus();
        }

        $fields = [
            'id' => $id,
            'user_id' => $user_id,
            'stripe_auth_id' => $stripe_auth_id,
            'stripe_data' => $stripe_data,
            'paypal_auth' => $paypal_auth,
            'paypal_data' => $paypal_data,
            'answer' => $answer,
            'amount' => $amount,
            'values' => $values,
            'notes' => $notes,
            'status' => $status
        ];

        return $fields;
    }

    public function getAdditionalData($_locale = 'en', &$fields, $parentid="null")
    { 
        $fields['users'] = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();
        if(empty($fields['subcategories']))
            $fields['subcategories'] = $this->getDoctrine()->getRepository('AppBundle:Subcategory')->findAll();
    }

    public function getSuccessMessage($create = true)
    {
        if($create)
            return "The transaction has been created successfully.";
        else
            return "The transaction has been updated successfully.";
    }
    
}
