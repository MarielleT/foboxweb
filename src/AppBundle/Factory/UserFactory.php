<?php

namespace AppBundle\Factory;

use CYINT\ComponentsPHP\Classes\ParseData;
use AppBundle\Entity\User;

class UserFactory extends PhotoATMMasterFactory
{ 
    protected $fieldKeys = ['username', 'role', 'plain_password', 'enabled'];
    protected $EntityType = 'User';

    public function __construct($Repository, $Doctrine, $Manager)
    {
        $this->setDoctrine($Doctrine);
        //Case mut match that used in the getter and setter method as 'get'and 'set' wll be appended to the keys.
         $this->setFields([
            'Id' => $this->initializeField(
                'none', null, null, null, null
            )
            ,'Username' => $this->initializeField(
                'text', 'Username/Email','','',['required'] 
            )
            ,'Role' => $this->initializeField(
                'select', 'Role', 'ROLE_ADMIN', 'ROLE_ADMIN', ['required']
                ,$this->getRoleOptions()
            )
            ,'PlainPassword' => $this->initializeField(
                'password', 'Password', null, null
            )
            ,'Enabled' => $this->initializeField(
                'checkbox', 'Active', false, false, [], ['getter'=>'isEnabled']
            )
        ]);

        parent::__construct($Repository, $Doctrine, $Manager);
    }

    public function entityEditUnique(&$User)
    {

    } 
    
    public function persistData($User)
    {
        $this->Manager->updateUser($User);
    }

    public function getSuccessMessage($create = true)
    {
        if($create)
            return "The user has been created successfully.";
        else
            return "The user has been updated successfully.";
    }

    public function getExceptionMessage(\Exception $Ex = null)
    {    
        switch(get_class($Ex))
        {
            case 'Doctrine\DBAL\Exception\UniqueConstraintViolationException':
                return 'A user with this user name already exists, and user names must be unique. Please enter a new user name and try again.';
            break;

            default:
                return $Ex->getMessage();
            break; 
        }
    }    
 
    private function getRoleOptions()
    {
        return [
            'selectOptions' => [
                'type' => 'static'
                ,'options' => [
                    'Admin' => 'ROLE_ADMIN'
                    ,'User' => 'ROLE_AUTHENTICATED'
                ]
            ]
        ];
    }

   
}
