<?php

namespace AppBundle\Entity;

/**
 * Transaction
 */
class Transaction
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $total;
    private $paid;
    private $photos;   
    private $cart_data;
    private $created;
    private $ended;
    private $status;
    private $notes;
    private $Machine;
    private $bills_inserted;
    private $change_amount;
    private $change_delivered;
    
    const STATUS_PENDING = 0;
    const STATUS_PAID = 1;
    const STATUS_DISPENSING_CHANGE = 2;
    const STATUS_ABANDONED = 3;
    const STATUS_PRINTED = 5;
    const STATUS_ADMINISTRATIVE = 6;
    const STATUS_REFUND_ISSUED = 7;

    public function __construct(Machine $Machine)
    {
        $this->setMachine($Machine);
        $this->setCreated(time());
        $this->setStatus(self::STATUS_PENDING);
        $this->setTotal(0); 
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set total
     *
     * @param string $total
     *
     * @return Transaction
     */
    public function setTotal($total)
    {
        $this->total = (float)$total;
        return $this;
    }

    /**
     * Get total
     *
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set created
     *
     * @param integer $created
     *
     * @return Transaction
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * Get created
     *
     * @return int
     */
    public function getCreated()
    {
        return $this->created;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getNotes()
    {
        return $this->notes;
    }

    public function setNotes($notes)
    {
        $this->notes;
        return $this;
    }

    public function setMachine(Machine $Machine)
    {
        $this->Machine = $Machine;
        return $this;
    }

    public function getMachine()
    {
        return $this->Machine;
    }

    public function setPaid($paid)
    {
        $this->paid = $paid;
        return $this;
    }

    public function getPaid()
    {
        return $this->paid;
    } 

    public function getBillsInserted()
    {
        if(!empty($this->bills_inserted))
            return json_decode($this->bills_inserted);

        return $this->bills_inserted;
    }

    public function setBillsInserted($bills_inserted)
    {
        if(!empty($bills_inserted))
            $this->bills_inserted = json_encode($bills_inserted);
        else
            $this->bills_inserted = $bills_inserted;

        return $this;
    }

    public function getEnded()
    {
        return $this->ended;
    }

    public function setEnded($ended)
    {
        $this->ended = $ended;
        return $this;
    }

    public function getChangeAmount()
    {
        return $this->change_amount;
    }

    public function setChangeAmount($change_amount)
    {
        $this->change_amount = $change_amount;
        return $this;
    }

    public function getChangeDelivered()
    {
        if(!empty($this->change_delivered))
            return json_decode($this->change_delivered);
        
        return $this->change_delivered;
    }

    public function setChangeDelivered($change_delivered)
    {
        if(!empty($change_delivered))
            $this->change_delivered = json_encode($change_delivered);
        else
            $this->change_delivered = $change_delivered;

        return $this;
    }

    public function getCartData()
    {
        if(!empty($this->cart_data))
            return json_decode($this->cart_data, true);

        return $this->cart_data;
    }

    public function getFormattedCartData()
    {
        $cart_data = [];
        $formatted_data = [];
        if(!empty($this->cart_data))
            $cart_data = json_decode($this->cart_data, true);
        else
            return null;

        foreach($cart_data["media"] as $mediaid=>$image)
        {
            if($image['quantity'] > 0) 
            {
                foreach($image["images"] as $key=>$value)
                {               
                    $formatted_data[] = [
                        'mediaid' => $mediaid
                        ,'image' => $value['standardres']
                        ,'rotation' => empty($image['rotation'][$key]) ? 0 : $image['rotation'][$key]
                        ,'orientation' => empty($image['orientation'][$key]) ? 'portrait-text' : $image['orientation'][$key]
                        ,'data' => empty($image['data'][$key]) ? [] : $image['data'][$key]
                    ];
                }
            }           
        }

        return $formatted_data;
    }

    public function setCartData($cart_data)
    {
        if(!empty($cart_data))
            $this->cart_data = json_encode($cart_data);
        else
            $this->cart_data = $cart_data;

        return $this;
    }

    public function toArray()
    {
        return [
            'id' => $this->getId()
            ,'total' => $this->getTotal()
            ,'paid' => $this->getPaid()
            ,'cart_data' => $this->getCartData()
            ,'created' => $this->getCreated()
            ,'ended' => $this->getEnded()
            ,'status' => $this->getStatus()
            ,'notes'=> $this->getNotes()
            ,'bills_inserted' => $this->getBillsInserted()
            ,'change_amount' => $this->getChangeAmount()
            ,'change_delivered' => $this->getChangeDelivered()
        ];
    }
}  


