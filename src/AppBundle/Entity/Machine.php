<?php

namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Transaction
 */
class Machine
{
    /**
     * @var int
     */
    private $id;
    private $address;
    private $city;
    private $postal_code;
    private $province;
    private $country;
    private $notes;
    private $created;
    private $ip_address;
    private $change_remaining;  
    private $space_remaining;
    private $status;
    private $transactions;
    private $paper;

    const STATUS_SETUP_MODE     = 0;
    const STATUS_NOCHANGE       = 1;
    const STATUS_FULL           = 2;
    const STATUS_FAILED_CHECKIN = 3; 
    const STATUS_NO_PAPER       = 4;
    const STATUS_PRINTER_ERROR  = 5;
    const STATUS_DECOMISSIONED  = 6;
    const STATUS_RUNNING        = 7;

    public function __construct()
    {
        $this->setCreated(time());
        $this->setTransactions(new ArrayCollection());
        $this->setStatus([Machine::STATUS_SETUP_MODE]);
    }
    /*     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getPostalCode()
    {
        return $this->postal_code;
    }

    public function setPostalCode($postal_code)
    {
        $this->postal_code = $postal_code;
        return $this;
    }

    public function setNotes($notes)
    {
        $this->notes = $notes;
        return $this;
    }

    public function getNotes()
    {
        return $this->notes;
    }

    public function setCountry($country)    
    {
        $this->country = $country;
        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    } 

    public function getCreated()
    {
        return $this->created;
    }

    public function setProvince($province)
    {
        $this->province = $province;
        return $this;
    }

    public function getProvince()
    {
        return $this->province;   
    }   

    public function setTransactions(ArrayCollection $transactions)
    {
        $this->transactions = $transactions;
        return $this;
    }

    public function getTransactions()
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $Transaction)
    {
        $this->transactions->add($Transaction);
        return $this;
    }

    public function removeTransaction(Transaction $Transaction)
    {
        if($this->transactions->contains($Transaction))
            $this->transactions->remove($Transaction);
        return $this;
    }

    public function setBills($bills)
    {
        if(!empty($bills))            
            $this->bills = json_encode($bills);
        else
            $this->bills = $bills;

        return $this;
    }

    public function getBills()
    {
        if(!empty($this->bills))
            return json_decode($this->bills);
        else
            return $this->bills;   
    }

    public function getIPAddress()
    {
        return $this->ip_address;
    }

    public function setIPAddress($ip_address)
    {
        $this->ip_address = $ip_address;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getChangeRemaining()
    {
        return $this->change_remaining;
    }

    public function setChangeRemaining($change_remaining)
    {
        $this->change_remaining = $change_remaining;
        return $this;
    }

    public function getSpaceRemaining()
    {
        return $this->space_remaining;
    }

    public function setSpaceRemaining($space_remaining)
    {
        $this->space_remaining = $space_remaining;
        return $this;
    }

    public static function getStatusOptions()
    {
        return [
            'SETUP MODE' => 0
            ,'NO CHANGE REMAINING' => 1
            ,'BILL COLLECTOR FULL' => 2
            ,'FAILED LAST CHECK-IN' => 3
            ,'NO PAPER' => 4
            ,'PRINTER ERROR' => 5
            ,'DECOMMISSIONED' => 6
            ,'RUNNING' => 7
        ];
    }

    public function setPaper($paper)
    {
        $this->paper = $paper;
    }

    public function getPaper()
    {
        return $this->paper;
    }

    public function toArray()
    {
        return [
            'id'=>$this->getId()
            ,'address'=>$this->getAddress()
            ,'city' => $this->getCity()
            ,'postal_code' => $this->getPostalCode()
            ,'province' => $this->getProvince()
            ,'country' => $this->getCountry()
            ,'notes' => $this->getNotes()
            ,'created' => $this->getCreated()
            ,'ip_address' => $this->getIPAddress()
            ,'change_remaining' =>$this->getChangeRemaining()
            ,'space_remaining' =>$this->getSpaceRemaining()
            ,'status' => $this->getStatus()  
            ,'paper' => $this->getPaper()
        ];
    }
}

