<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\WebDriverBy;
use AppBundle\Entity\Dispatch;
use AppBundle\Entity\Setting;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
class ScreenshotCommand extends ContainerAwareCommand
{
    protected $input;
    protected $output;
	protected $timeout_time = 20;
	protected $exceptions;
    protected $settings;
    protected $Dispatch;
	protected $slot;
	protected $transaction; 
    protected $router;

    protected function configure()
    {
        $this
		 	->setName('emoji:screenshot')
            ->setDescription('Take a screenshot of emoji text so it can be served as an image in a PDF')
            ->addArgument(
                'transaction',
                InputArgument::REQUIRED,
                'The transaction id associated with the images to be processed'
            )
            ->addArgument(
                'slot',
                InputArgument::REQUIRED,
                'The specific image slot to process'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try 
        {
            $this->input = $input;
            $this->output = $output;
            $this->exceptions = [];
			$this->transaction = $this->input->getArgument('transaction');
			$this->slot = $this->input->getArgument('slot');			
            $this->Doctrine = $this->getContainer()->get('doctrine');
            $this->settings = $this->Doctrine->getRepository('CYINTSettingsBundle:Setting')->findByNamespace('aws');       
            $this->router = $this->getContainer()->get('router');
			$this->coreCommand();
        }
        catch (\Exception $e)
        {
            print_r($e->getMessage() . " " .  $e->getLine());
            $exceptions[] = $e;

        }      
    }

	protected function coreCommand()
	{
		$this->request($this->settings['screenshot_endpoint'],json_encode([
			'render_url'=> $this->settings['photoatm_host'] . $this->router->generate('render_html', ['transaction'=>$this->transaction])
			,'transaction'=> $this->transaction
	        ,'selector'=> '#caption_' . $this->slot
            ,'destination'=> $this->transaction . '_' . $this->slot . '.png'
		]));
	}

   	protected function request($api, $post_data = null, $return_transfer = true)
    {             
		$process = curl_init();
        curl_setopt($process, CURLOPT_URL, $api);
        curl_setopt($process, CURLOPT_CUSTOMREQUEST, 'POST'); 
        curl_setopt($process, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Content-Length: ' . strlen($post_data)]); 
		curl_setopt($process, CURLOPT_HEADER, false);
		curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, $return_transfer);
		if(!empty($post_data))
		{
			curl_setopt($process,CURLOPT_POSTFIELDS, $post_data);	           
		}

        $this->process = $process;
        if($return_transfer)
        {
            $response = curl_exec($this->process);
            return $response;
        }
        else
            curl_exec($this->process);
	} 

}
