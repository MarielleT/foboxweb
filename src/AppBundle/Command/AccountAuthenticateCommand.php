<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\WebDriverBy;
use AppBundle\Entity\Dispatch;
use AppBundle\Entity\Setting;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
class AccountAuthenticateCommand extends ContainerAwareCommand
{
    protected $input;
    protected $output;
	protected $timeout_time = 20;
	protected $exceptions;
    protected $settings;
    protected $url;

    protected function configure()
    {
        $this
		 	->setName('account:authenticate')
            ->setDescription('Authenticate account and get new access token')
            ->addArgument(
                'instagram_url',
                InputArgument::REQUIRED,
                'The instagram authentication endpoint'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try 
        {
            $this->input = $input;
            $this->output = $output;
            $this->url = $this->input->getArgument('instagram_url');
            $this->exceptions = [];
            $this->settings = $this->getContainer()->get('doctrine')->getRepository('CYINTSettingsBundle:Setting')->findByNamespace('');       
			$this->coreCommand();
        }
        catch (\Exception $e)
        {
            print_r($e->getMessage() . " " .  $e->getLine());
            $exceptions[] = $e;

        }      
    }

	protected function coreCommand()
	{
        $post_data = json_encode([
			'username'=> $this->settings['instagrambaseusername']
            ,'password'=>$this->settings['instagrambasepassword']
            ,'url'=>$this->url
		]);
        
		$response = $this->request($this->settings['awsreauthenticationendpoint'],$post_data);        
	}

   	protected function request($api, $post_data = null, $return_transfer = true)
    {             
		$process = curl_init();
        curl_setopt($process, CURLOPT_URL, $api);
        curl_setopt($process, CURLOPT_CUSTOMREQUEST, 'POST'); 
        curl_setopt($process, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Content-Length: ' . strlen($post_data)]); 
		curl_setopt($process, CURLOPT_HEADER, false);
		curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, $return_transfer);
		if(!empty($post_data))
		{
			curl_setopt($process,CURLOPT_POSTFIELDS, $post_data);	           
		}

        $this->process = $process;
        if($return_transfer)
        {
            $response = curl_exec($this->process);
            return $response;
        }
        else
            curl_exec($this->process);
	} 

}
