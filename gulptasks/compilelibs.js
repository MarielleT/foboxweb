const gulp = require('gulp');
const replace = require('gulp-replace');

gulp.task('compilelibs',['delete'], function () {
   
   gulp
    .src(['./node_modules/bootstrap/dist/**/*'])
    .pipe(gulp.dest('./web/dist/lib/bootstrap'));

   gulp
    .src(['./node_modules/jquery/dist/**/*'])
    .pipe(gulp.dest('./web/dist/lib/jquery'));

   gulp
    .src(['./node_modules/moment/min/moment.min.js'])
    .pipe(gulp.dest('./web/dist/lib/moment'));

   gulp
    .src(['./node_modules/moment-timezone/builds/moment-timezone-with-data-2010-2020.min.js'])
    .pipe(gulp.dest('./web/dist/lib/moment-timezone'));

    gulp
    .src(['./node_modules/ckeditor/**/*'])
    .pipe(gulp.dest('./web/dist/lib/ckeditor/'));

    gulp
    .src(['./node_modules/smoothstate/src/**/*'])
    .pipe(gulp.dest('./web/dist/lib/smoothstate/'));

    gulp
    .src(['./node_modules/kinetic/2/*'])
    .pipe(gulp.dest('./web/dist/lib/kinetic/'));

    gulp
    .src(['./node_modules/jquery-emoji-picker/*/**'])
    .pipe(gulp.dest('./web/dist/lib/jquery-emoji-picker/'));

    gulp
    .src(['./node_modules/virtual-keyboard/dist/*/**'])
    .pipe(gulp.dest('./web/dist/lib/virtual-keyboard/'));

    gulp
    .src(['./node_modules/croppie/**/*'])
    .pipe(gulp.dest('./web/dist/lib/croppie/'));


    gulp
    .src(['vendor/iamcal/php-emoji/lib/emoji.png'])
    .pipe(gulp.dest('./web/dist/img'));

    gulp
    .src(['vendor/iamcal/php-emoji/lib/emoji.css'])
	.pipe(replace('emoji.png','/dist/img/emoji.png'))
    .pipe(gulp.dest('./web/dist/css'));


    return true;
});
